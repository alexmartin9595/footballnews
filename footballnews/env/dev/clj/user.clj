(ns user
  (:require [mount.core :as mount]
            footballnews.core))

(defn start []
  (mount/start-without #'footballnews.core/repl-server))

(defn stop []
  (mount/stop-except #'footballnews.core/repl-server))

(defn restart []
  (stop)
  (start))


