(ns footballnews.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [footballnews.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[footballnews started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[footballnews has shut down successfully]=-"))
   :middleware wrap-dev})
