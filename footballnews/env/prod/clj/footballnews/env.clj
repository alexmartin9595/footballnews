(ns footballnews.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[footballnews started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[footballnews has shut down successfully]=-"))
   :middleware identity})
